const Canvas={
    UI: {
        /**
         * Creates a flash message.
         * @param text{string} The content of the flash message.
         * @param opt{{
           style: "info"|"error"|"warning"|"success",
           icon: "info"|"x"|"warning"|"check"|"lock"|"rss"|"feed"|"shield"|"standards"
           }}
         */
        createFlashMessage: function(text,opt) {
            var options={
                icon: 'info',
                style: 'info'
            };
            try {
                if(opt.style==="info"||opt.style==='error'||opt.style==='warning'||opt.style==='success') {
                    options.style=opt.style;
                }
                if(opt.icon) {
                    switch(opt.icon) {
                        case 'info':
                        default:
                          options.icon='info'
                          break
                        case 'x':
                          options.icon='trouble'
                          break
                        case 'warning':
                          options.icon='warning'
                          break
                        case 'check':
                          options.icon='check'
                          break
                        case 'lock':
                          options.icon='lock'
                          break
                        case 'rss':
                        case 'feed':
                          options.icon='rss'
                          break
                        case 'shield':
                        case 'standards':
                          options.icon='standards'
                          break
                        case 'trash':
                          options.icon='trash'
                          break
                        case 'target':
                          options.icon='target'
                          break
                        case 'quiz':
                        case 'rocket':
                          options.icon='quiz'
                          break
                        case 'unread':
                          options.icon='next-unread'
                          break
                        case 'progress':
                        case 'refresh':
                          options.icon='progress'
                          break
                        case 'question':
                          options.icon='question'
                          break
                    }
                }
            } catch (error) {
                options={
                    icon: 'info',
                    style: 'info'
                }
            }
            var flash=document.createElement('div');
            flash.className=`flash-message-container ic-flash-${options.style}`;
            flash.style.zIndex='2';
            flash.ariaHidden='true';
            var iconc=document.createElement('div');
            iconc.className="ic-flash__icon";
            var icon=document.createElement('i');
            icon.className=`icon-${options.icon}`;
            iconc.appendChild(icon);
            flash.appendChild(iconc);
            var msg=document.createTextNode(text);
            flash.append(msg);
            var closeb=document.createElement('button');
            closeb.className="Button Button--icon-action close_link";
            closeb.type='button';
            closeb.ariaLabel="Close";
            var close=document.createElement('i');
            close.className="icon-x";
            closeb.appendChild(close);
            flash.appendChild(closeb);
            document.querySelector("#flash_message_holder").appendChild(flash);
        }
    },
    assignments: {
        submitTextEntry: function (course, assignment,textEntryData,comments) {
            var xhr = new XMLHttpRequest();
            xhr.open("POST",`/courses/${course}/assignments/${assignment}/submissions`);
            xhr.setRequestHeader("Content-type",'application/x-www-urlencoded');
            var paramaters = {
              'submission[submission_type]': 'online_text_entry',
              'submission[comments]':comments||"",
              'submission[eula_agreement_timestamp]': (new Date()).toString(),
              'submission[body]': textEntryData,
              'utf': '✓'
            }
            var a=[];
            Object.keys(paramaters).forEach((e)=>{
              a.push(`${encodeURIComponent(e)}=${encodeURIComponent(paramaters[e])}`)
            })
            xhr.send(`?${a.join("&")}`)
        }
    }
}

